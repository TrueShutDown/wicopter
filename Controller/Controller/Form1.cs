﻿
using System;
using System.Windows.Forms;
using SlimDX.XInput;
using System.Net;
using System.Net.Sockets;

namespace Controller
{
    public partial class Form1 : Form
    {
        GamepadState gamepad;
        Socket socket;
        IPEndPoint ep;
        IPAddress broadcast;

        bool connected = false;

        public Form1()
        {
            InitializeComponent();

            gamepad = new GamepadState(UserIndex.One);
            timer1.Start();

        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            const Byte BIT_1 = 128;
            const Byte BIT_2 = 64;
            const Byte BIT_3 = 32;
            const Byte BIT_4 = 16;
            const Byte BIT_5 = 8;
            const Byte BIT_6 = 4;
            const Byte BIT_7 = 2;
            const Byte BIT_8 = 1;

            gamepad.Update();

            float[] PP = new float[5];

            PP[0] = (gamepad.LeftStick.Position.X + 1) * 0.5f;
            PP[1] = 0.65f * gamepad.LeftTrigger + 0.35f * gamepad.RightTrigger;
            PP[2] = (gamepad.RightStick.Position.X + 1) * 0.5f;
            PP[3] = (gamepad.RightStick.Position.Y + 1) * 0.5f;
            PP[4] = (gamepad.LeftStick.Position.Y + 1) * 0.5f;
            
            //message[4] = 0;
            /*if (gamepad.LeftShoulder) message[4] |= BIT_1;
            if (gamepad.RightShoulder) message[4] |= BIT_2;
            if (gamepad.Back) message[4] |= BIT_3;
            if (gamepad.Start) message[4] |= BIT_4;
            if (gamepad.A) message[4] |= BIT_5;
            if (gamepad.B) message[4] |= BIT_6;
            if (gamepad.X) message[4] |= BIT_7;
            if (gamepad.Y) message[4] |= BIT_8;*/

            Byte[] message = new Byte[5];
            for (int i = 0; i < 5; i++)
            {
                message[i] = (Byte)Math.Round((PP[i] * 100 + 100));
            }

            label1.Text = message[0].ToString();
            label2.Text = message[1].ToString();
            label3.Text = message[2].ToString();
            label4.Text = message[3].ToString();
            label5.Text = message[4].ToString();

            if (connected)
            {
                socket.SendTo(message, ep);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Disconnect")
            {
                connected = false;

                button1.Text = "Connect";

                button1.ForeColor = System.Drawing.Color.Black;

                textBox1.ReadOnly = false;
                textBox2.ReadOnly = false;
            }
            else if (button1.Text == "Connect")
            {
                string currentIP = textBox1.Text;
                string currentPORT = textBox2.Text;

                connected = true;

                button1.Text = "Disconnect";

                textBox1.ReadOnly = true;
                textBox2.ReadOnly = true;
                button1.ForeColor = System.Drawing.Color.Green;

                try
                {
                    socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    broadcast = IPAddress.Parse(currentIP);
                    ep = new IPEndPoint(broadcast, Int32.Parse(currentPORT));
                }
                catch
                {
                    button1.ForeColor = System.Drawing.Color.Red;
                    connected = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Butoka Piotr\nChwesiuk Michał\nPaleń Dawid", "Authors");
        }
    }
}
