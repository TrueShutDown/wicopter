# README #

### Projekt ###

G��wnym celem przedstawionego zadania jest projekt i implementacja mechanicznej konstrukcji quadrocoptera o du�ej wytrzyma�o�ci i mo�liwie ma�ej masie, umo�liwiaj�cej w dalszej perspektywie wyposa�enie w aparatur� do pomiaru danych telemetrycznych oraz retransmisji sygna�u akustycznego.
Dodatkowym aspektem projektowym jest wykorzystanie druku 3D do wytworzenia element�w mechanicznych (np. ramy quadrocoptera) oraz por�wnanie wydajno�ci r�nych �r�de� zasilania przede wszystkim pod k�tem kosztu oraz czasu pracy.
Jako jednostka steruj�ca zosta� wykorzystany komputer jednop�ytkowy Orange Pi Lite wraz z kontrolerem lotu CC3D. Do obs�ugi sterowania i wymiany danych wykorzystany zosta� systemem operacyjny Linux Armbian. Umo�liwia to znaczne rozszerzenie funkcjonalno�ci ca�ego systemu oraz stwarza mo�liwo�� elastycznego instalowania i uruchamiania niezb�dnego oprogramowania.
Sterowanie realizowane jest np.: ze smartfonu, tabletu lub komputera wyposa�onego w modu� WiFi z zainstalowanym systemem Windows, Linux lub Android.


### Strona projektu ###
* [Strona projektu](http://www.wiwav.zut.edu.pl/apl/)
