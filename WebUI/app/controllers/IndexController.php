<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        // Add some local CSS resources
//        $this->assets->addCss('css/style.css');
        $this->assets->addCss('components/bootstrap/dist/css/bootstrap.min.css');
        $this->assets->addCss('components/bootstrap/dist/css/bootstrap-theme.min.css');
        $this->assets->addCss('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

        // And some local JavaScript resources
//        $this->assets->addJs('js/jquery.js');
        $this->assets->addJs('components/jquery/dist/jquery.min.js');
        $this->assets->addJs('components/bootstrap/dist/js/bootstrap.min.js');
        $this->assets->addJs('js/core.js');
    }

}

