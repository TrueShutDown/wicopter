let ws_client = {
    url: 'echo.websocket.org/',
    websocket: null,
    console: $('#console'),
    console_input: $('#console_input'),
    init: function () {

        let self = this;

        this.websocket = new WebSocket("ws://" + this.url);

        this.websocket.onopen = function () {
            self.consoleWrite('Connected!', 'system');
            // document.getElementById("output").innerHTML += "<p>> CONNECTED</p>";
        };

        this.websocket.onmessage = function (evt) {
            self.consoleWrite(evt.data, 'receive');
            // document.getElementById("output").innerHTML += "<p style='color: blue;'>> RESPONSE: " + evt.data + "</p>";
        };

        this.websocket.onerror = function (evt) {
            self.consoleWrite(evt.data, 'error');
        };


        this.console_input.keypress(function (e) {
            if (e.which == 13) {

                let data = self.console_input.val();
                self.send(data);
                self.console_input.val('');

                // return false;
            }
        });
    },


    send: function (message) {
        this.consoleWrite(message, 'sended');
        this.websocket.send(message);
    },

    consoleWrite: function (msg, type) {

        msg = '<span class="time">' + new Date().toLocaleString() + '</span>' + '<span class="console_separator"> ></span> ' + '<span class="msg">' + msg + '</span>';
        if (typeof type === 'undefined') {
            msg = '<div class="console_normal console_line">' + msg + ' </div>';
        }

        if (type === 'system') {
            msg = '<div class=" console_system console_line">' + msg + ' </div>';
        }

        if (type === 'receive') {
            msg = '<div class="console_receive console_line">' + msg + ' </div>';
        }

        if (type === 'sended') {
            msg = '<div class="console_sended console_line">' + msg + ' </div>';
        }

        if (type === 'error') {
            msg = '<div class="console_error console_line">' + msg + ' </div>';
        }

        // debugger;
        this.console.append(msg);
    }
};


$(document).ready(function () {
    ws_client.init();
});