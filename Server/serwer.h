#pragma once
#include "inc.h"

class Server
{
public:
  Server();
  void init();
  int update();
  void closeSocket();
  void die(std::string s);
  char buf[BUFLEN];
  bool isRunning();


private:
  struct sockaddr_in si_me, si_other;
  int s, i , recv_len;

  bool m_running;

  socklen_t slen;
};

void Server::die(std::string s)
{
    //perror(s);
    exit(1);
}

Server::Server()
{
	m_running = false;
  std::cout << "Server started!" << std::endl;
}


void Server::init()
{

  //printf("init 1\n");
  slen = sizeof(si_other);
  if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
  {
      die("socket");
  }
  //printf("init 2\n");
  // zero out the structure
  memset((char *) &si_me, 0, sizeof(si_me));

  si_me.sin_family = AF_INET;
  si_me.sin_port = htons(PORT);
  si_me.sin_addr.s_addr = htonl(INADDR_ANY);
  //printf("init 3\n");
  //bind socket to port
  if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
  {
      die("bind");
  }
  //printf("init 4\n");
  m_running = true;
}

int Server::update()
{
  // while(1)
  // {
      // printf("Waiting for data...");
      // fflush(stdout);
      //printf("update 1\n");
      if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == -1)
      {
      //  printf("update 2\n");
          // die("recvfrom()");
		  m_running = false;
		  return 0;
      }
      //printf("update 3\n");
	  return 1;

      // int i;
      // for(i = 0 ; i < 5 ; i++)
        // printf("%d ", buf[i]);
		// printf("\n");

  // }
  // close(s);
}

void Server::closeSocket()
{
	close(s);
}

bool Server::isRunning()
{
	return m_running;
}
