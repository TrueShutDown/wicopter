#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <wiringPi.h>
#include <curses.h>
#include <ncurses.h>
#include <sys/select.h>
#define	PORT 0


struct termios orig_termios;

void reset_terminal_mode()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
    struct termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);
    cfmakeraw(&new_termios);
    tcsetattr(0, TCSANOW, &new_termios);
}

int kbhit()
{
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv);
}

int getch2()
{
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}

int main(int argc, char *argv[])
{
	set_conio_terminal_mode();
	int c;
	float freq;
	freq	= 1;
	int counter;
	int delay_time;
	delay_time =30;
	float step;
	 wiringPiSetup () ;
	 pinMode (PORT, OUTPUT) ;
step 	= 0.01;
	    WINDOW * win =   initscr();
	        noecho();
					 printw("ESC Controller by SD\n-----------------\nkeys\nw: width frame +\ns: width frame -\nESC: END\n");
	while(1)
	{
		while (!kbhit()) 
		{
			counter++;
			delay (delay_time) ;
			digitalWrite (PORT, HIGH) ;	// On
			//delay (freq) ;		// mS
			usleep(freq * 1000);
			digitalWrite (PORT, LOW) ;	// Off
			if(counter%100 == 0 || counter == 1)
	  {
                move(10,0);
				printw("Send signal: %d, freq: %f, delay time: %d \n", counter, freq, delay_time);
				refresh();
	  }
	// send signals
		}
    // (void)getch(); /* consume the character */
    c = getch2(); /* consume the character */
	printf("%d", c);
	
		if( c == 27)
			{	 
				endwin();
				return 1;
			
			}
		  // timeout(-1);


			if(c == 119)
			{
				freq = freq + step;
			}
			
			if(c == 115)
			{
				freq = freq -step;
			}				
			
			if(c == 122 )
			{
				delay_time =delay_time -1;
			}
			
			if(c == 120)
			{
				delay_time = delay_time + 1;
			}		
			if(c == 48)
			{
				freq = 0;
			}			
			
			if(c == 49)
			{
				freq = 1;
			}			
			
			if(c ==50)
			{
				freq = 2;
			}
			
			if(c ==51)
			{
				freq = 3;
			}
                move(10,0);
					printw("Send signal: %d, freq: %f, delay time: %d \n", counter, freq, delay_time);
				refresh();
	
	
	

	}
			endwin();
	return 0;
}



/*

#include <stdio.h>
#include <wiringPi.h>
#include <curses.h>
// LED Pin - wiringPi pin 0 is BCM_GPIO 17.
#include <stdlib.h>  
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <termios.h>
#define	PORT 0

int main(int argc, char **argv)
{

float freq; 
// freq = atof(argv[1]);
// int wait_time;
int counter;

freq = 0;
// wait_time = 20;
// float step;




    WINDOW * win =   initscr();
						  		// cbreak();
nodelay(win, TRUE);
        noecho();
		notimeout(win,0);

        // raw();
        int c;
		

         // timeout(-1);
	   while(true)
        {

			c = getchar();
			if( c == 27)
			{	 
		endwin();
				return 1;
			
			}
		  // timeout(-1);

			counter ++;
			if(c == 119)
			{
				freq = freq +0.1;
			}
			
			if(c == 115)
			{
				freq = freq -0.1;
			}
                move(10,0);
                // printw("Keycode: %d, and the character: %c \n", c, c);
                // move(0,0);
                // printw("Write something (ESC to escape): ");
             
				
				  printw("Send signal: %d, freq: %f\n", counter, freq);
				     refresh();
        }
       
        return 0;
//counter = 0;
// wait_time = atoi(argv[2]);
  // printf ("ESC Controller freq: %f ms, wait time: %d ms \n", freq, wait_time) ;
// step = 0.1;
  // wiringPiSetup () ;
  // int key;
 
  // pinMode (LED, OUTPUT) ;

  // for (;;)
  // {    
// delay (wait_time) ;
	  // if(counter == 200)
	  // {
		
			// freq = 1 + step;
			// step = step + 0.1;
			// counter = 0;
			// printf ("run, freq: %f\n", freq) ;
	  // }
	 // counter++;
    // digitalWrite (PORT, HIGH) ;	// On
    // delay (freq) ;		// mS
    // digitalWrite (PORT, LOW) ;	// Off

  // }
  


}

*/
